'use strict';

/**
 * list of people (num)
 * Randomize into groups of 3-5 people
 * 
 */

const people = [];
for (let i = 0; i < 14; i++) {
    people.push(i);
}

let randomizeGroups = (arr) => {
    const toReturn = [];

    const len = arr.length;
    for (let i = 0; i < len; i++) {
        if (i % 3 === 0) {
            toReturn.push([]);
        }
        const randomPerson = arr.splice(Math.floor(Math.random() * arr.length), 1)
        toReturn[Math.floor(i / 3)].push(randomPerson[0]);
    }

    if (toReturn.length > 1 && toReturn[toReturn.length - 1].length < 3) {
        let group = toReturn.pop();
        for (let i = 0; i < group.length; i++) {
            toReturn[0].push(group[i]);
        }
    }

    console.log(toReturn);
    return toReturn;
};

let testA = () => {
    const result = randomizeGroups(people);
    for (let i = 0; i < result.length; i++) {
        if (result[i].length < 3 || result[i].length > 5) {
            throw new Error(`group ${i} has ${result[i].length} people in it`);
        }
    }
}

let testB = () => {
    const peopleB = [0, 1];
    const result = randomizeGroups(peopleB);
    if (result[0].length !== 2) {
        throw ('expected result to have 2 people');
    }
}

let testC = () => {
    const peopleC = [0];
    const result = randomizeGroups(peopleC);
    if (result[0].length !== 1) {
        throw ('expected result to have 1 people');
    }
}

testA();
testB();
testC();

console.log('All Tests pass!');